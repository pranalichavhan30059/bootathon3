# **Solution of optimization problems**
# Objective
The objective of this experiment is to provide a suboptimal solution to the Travelling Salesman Problem (TSP), using the properties of self-organization feature maps (SOM). The focus is:
- To illustrate the principle of self-organization for addressing the travelling salesman problem
- To observe the suboptimal nature of the solution provided by SOM
- To study the effect of structure of SOM on the solution
# Illustration
## Application of SOM to travelling salesman problem
Let us consider a SOM  network with 1000 neurons in the output layer, for the travelling salesman problem of 100 cities. The 2-dimensional input represents the coordinate values of a city. The units in the output layer are arranged (indexed) along a closed curve or ring. The weights vectors corresponding to adjacent units are joined in the weight space. In the following demonstration, the plots show the coordinates of the cities (marked by symbol 'x')  and the weight vectors (marked by symbol 'o'). The initialization of weight vectors along the rim of a ring is known as elastic ring approach in feature mapping. Plots show the closed path after 500, 1000 and 10000 iterations, respectively. Some of the cities may not be visited, which indicates the suboptimal nature of the algorithm.
## For 100 cities and a SOM with 1000 neurons in the output layer
![](http://cse22-iiith.vlabs.ac.in/exp8/images/agif-som-100cities-1000units.gif)
**Figure 1**: *Kohonen's self-organization feature map for TSP for 100 cities and 1000 units in the output layer*.
## Effect of varying the number of units in output layer of SOM
For the travelling salesman problem of 50 cities, we consider SOM networks with different number of units/nodes in the output layer. In the following demonstration, the plots show the coordinates of the cities (marked in black)  and the weight vectors (marked in red).  Plots show the closed path after 50, 100, 200 and 300 units in the output layer of SOM. Some of the cities may not be visited, which indicates the suboptimal nature of the algorithm.
## For 50 cities
![](http://cse22-iiith.vlabs.ac.in/exp8/images/agifTspDifferentNumberOfUnits.gif)
**Figure 2**: *Illustration of sub-optimal nature of the algorithm*
# Procedure
- Choose the number of input units to intialize the SOM. In this case, the input vector represents the coordinates of a city. So the input dimension is 2.
- Choose the number K of nodes/neurons in the output layer. If N denotes the number of cities, then K should be greater than or equal to N.
- Choose over the total number of iterations the SOM will go through. Each iteration involves adjustment of weights for all the neurons.
- Choose the step size for iteration results display purpose for number of cities i.e. units in the output network. Also choose the iteration step size for generation of output.
- Click on 'Next city' button or the 'Next Itern' button to run simulations.

# Experiment
## Self-organizing map for addressing travelling salesman problem
Click here: http://cse22-iiith.vlabs.ac.in/exp8/som.php

# Obeservations
- Observe the behaviour of the SOM network as a function of number of iterations. As the number of iterations increases, the weights of the SOM network align closer to the coordinates of the cities. Let us consider a SOM network with K-unit output layer and a 2-unit input layer. In this case, K=100 and N=30 (i.e., 30 cities and 100 neurons in the output layer). The 2-dimensional input represents the coordinate values of a city. The units in the output layer are arranged along a closed curve. The  weights vectors corresponding to adjacent units are joined to form a closed curve. In the following figure, plot (a) shows the coordinates of the cities (marked by symbol 'x')  and the weight vectors (marked by symbol 'o'). The initialization of weight vectors along the rim of a ring is known as elastic ring approach in feature mapping. Plots (b), (c) and (d) show the closed path after 200, 500 and 1000 iterations, respectively. Note that some of the cities may not be visited.
![](http://cse22-iiith.vlabs.ac.in/exp8/images/som-tsp-elastic-1.jpg)
- Observe the output of the network for different number of cities. Start from a small number of cities (such as 10), and go up to a large number of cities (such as 100 or more). Note that the solution provided by SOM is suboptimal, in the sense that coordinates of some cities may not be covered by the weights of the network. Observe this behaviour for varying number of cities.
- The variation of neighbourhood function for different iterations needs to be scheduled. A larger neighbourhood function is used initially, and the size of the neighbourhood is reduced progressively. The effect of this change can be observed.

# Assignment
Observe the performance of self-organizing map for travelling salesman problem for the following conditions:
- Number of cities = 100 and number of neurons in the output layer = 2000
- Number of cities = 100 and number of neurons in the output layer = 50
- In each case, compute the number of cities that have not been visited even once.
- Plot the percentage of cities visited, as a function of the number of iterations in either cases.
## References
- B. Yegnanarayana, Artificial Neural Networks, New Delhi, India : Prentice-Hall of India, p. 298, 1999.
- T. Kohonen, "Analysis of simple self-organizing process", Biol. Cybernet., vol. 44, pp. 135-140, 1982a.
- J.J. Hopfield and D.W. Tank, "Neural computation of decisions in optimization problems", Biol. Cybernet., vol. 52, pp. 141-152, 1985.



